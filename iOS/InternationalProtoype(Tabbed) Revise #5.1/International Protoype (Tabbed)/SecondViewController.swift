//
//  SecondViewController.swift
//  International Protoype (Tabbed)
//
//  Created by CSE455User on 5/4/15.
//  Copyright (c) 2015 CSE455User. All rights reserved.
//  Advising page 

import UIKit

class SecondViewController: UIViewController {

   // @IBOutlet weak var AcademicTextView: UITextView!
    
    @IBOutlet weak var ProgramsAndCoursesButton: UIButton!
    
    @IBOutlet weak var OrientationAndTestingButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.navigationController?.navigationBar.translucent = false
        
        
        let image = UIImage(named: "CISP_Banner")
        let imageView = UIImageView(image: image)
        
        imageView.frame = CGRectMake(0, 0, 300, 40)
        
        self.navigationItem.titleView = imageView
        
        //        
      //  AcademicTextView.contentOffset = CGPointMake(0, -220)
        
        ProgramsAndCoursesButton.layer.cornerRadius = 5
        OrientationAndTestingButton.layer.cornerRadius = 5
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

