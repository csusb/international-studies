//
//  FifthViewController.swift
//  International Protoype (Tabbed)
//
//  Created by CSE455User on 5/4/15.
//  Copyright (c) 2015 CSE455User. All rights reserved.
//  Exchange Program page 


import UIKit

class FifthViewController: UIViewController {

    @IBOutlet weak var ExchangeProgramTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let image = UIImage(named: "CISP_Banner")
        self.navigationController?.navigationBar.setBackgroundImage(image, forBarMetrics: .Default)
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        ExchangeProgramTextView.contentOffset = CGPointMake(0, -220)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
