//
//  NewsWebViewController.swift
//  International Protoype (Tabbed)
//
//  Created by Mark Martinez on 5/3/16.
//  Copyright © 2016 CSE455User. All rights reserved.
//

import UIKit

class NewsWebViewController: UIViewController {

    @IBOutlet var webview: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let url = NSURL(string: "http://news.csusb.edu")
        let requestObj = NSURLRequest(URL: url!)
        webview.loadRequest(requestObj)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func goBack(_: AnyObject) {
        if webview.canGoBack {
            webview.goBack()
        }
        else {
            dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    
    @IBAction func goForward(_: AnyObject) {
        webview.goForward()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
