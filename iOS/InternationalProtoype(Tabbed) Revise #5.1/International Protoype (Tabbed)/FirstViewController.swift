//
//  FirstViewController.swift
//  International Protoype (Tabbed)
//
//  Created by CSE455User on 5/4/15.
//  Copyright (c) 2015 CSE455User. All rights reserved.
//
//About us page 

import UIKit

class FirstViewController: UIViewController {

    @IBOutlet weak var StaffButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let image = UIImage(named: "CISP_Banner")
        self.navigationController?.navigationBar.setBackgroundImage(image, forBarMetrics: .Default)
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        StaffButton.layer.cornerRadius = 5

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

