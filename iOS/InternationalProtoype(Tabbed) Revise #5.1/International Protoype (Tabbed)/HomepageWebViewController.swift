//
//  HomepageWebViewController.swift
//  International Protoype (Tabbed)
//
//  Created by Mark Martinez on 4/28/16.
//  Copyright © 2016 CSE455User. All rights reserved.
//

import UIKit

class HomepageWebViewController: UIViewController {
    
    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let url = NSURL(string: "http://international.csusb.edu")
        let request = NSURLRequest(URL: url!);
        webView.loadRequest(request)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func goBack(_: AnyObject) {
        if webView.canGoBack {
            webView.goBack()
        }
        else {
            dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    
    @IBAction func goForward(_: AnyObject) {
        webView.goForward()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
