//
//  GilmanScholarship.swift
//  International Protoype (Tabbed)
//
//  Created by Eddie on 5/23/15.
//  Copyright (c) 2015 CSE455User. All rights reserved.
//

import UIKit

class GilmanScholarship: UIViewController
{
    @IBOutlet weak var GilmanTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        GilmanTextView.contentOffset = CGPointMake(0, -220)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
