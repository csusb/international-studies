//
//  FindHealthInsurance.swift
//  International Protoype (Tabbed)
//
//  Created by Eddie on 5/23/15.
//  Copyright (c) 2015 CSE455User. All rights reserved.
//

import UIKit

class FindHealthInsurance: UIViewController
{
    @IBOutlet weak var healthText: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        healthText.contentOffset = CGPointMake(0, -220)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
