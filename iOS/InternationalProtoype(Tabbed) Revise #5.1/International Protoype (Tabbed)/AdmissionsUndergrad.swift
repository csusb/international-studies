//
//  AdmissionsUndergrad.swift
//  International Protoype (Tabbed)
//
//  Created by user26236 on 5/31/15.
//  Copyright (c) 2015 CSE455User. All rights reserved.
//
import UIKit

class AdmissionsUndergrad: UIViewController
{
    @IBOutlet weak var adminUndergradpdfWebView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        let pdfPath:String? = NSBundle.mainBundle().pathForResource("admissionsUndergrad", ofType: "pdf")!
        let url = NSURL(fileURLWithPath: pdfPath!)
        let pdfRequest = NSURLRequest(URL: url)
        self.adminUndergradpdfWebView.loadRequest(pdfRequest)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}