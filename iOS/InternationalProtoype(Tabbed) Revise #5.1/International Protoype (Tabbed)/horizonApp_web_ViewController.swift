//
//  horizonApp_web_ViewController.swift
//  International Protoype (Tabbed)
//
//  Created by CSE455User on 6/14/15.
//  Copyright (c) 2015 CSE455User. All rights reserved.
//

import UIKit

class horizonApp_web_ViewController: UIViewController {

    @IBOutlet var webview: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let url = NSURL(string: "https://csusb-horizons.symplicity.com/index.php?au=&ck=")
        let requestObj = NSURLRequest(URL: url!)
        webview.loadRequest(requestObj)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func backFnc(sender: AnyObject) {
        if webview.canGoBack {
            webview.goBack()
        }
        else {
            dismissViewControllerAnimated(true, completion: nil)
        }
    }
  
    @IBAction func forwardFnc(sender: AnyObject) {
        webview.goForward()
    }

}
