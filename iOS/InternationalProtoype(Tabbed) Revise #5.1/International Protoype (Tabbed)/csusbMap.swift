//
//  csusbMap.swift
//  International Protoype (Tabbed)
//
//  Created by Braden Archibald on 6/8/15.
//  Copyright (c) 2015 CSE455User. All rights reserved.
//


import UIKit

class csusbMap: UIViewController
{
    

    @IBOutlet weak var campusMap: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let mbaProgramPath:String? = NSBundle.mainBundle().pathForResource("csusbMap1", ofType: "pdf")!
        let url = NSURL(fileURLWithPath: mbaProgramPath!)
        let pdfRequest = NSURLRequest(URL: url)
        self.campusMap.loadRequest(pdfRequest)
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButton(sender: UIBarButtonItem) {
        dismissViewControllerAnimated(true, completion: nil)
    }
}