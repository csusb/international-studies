//
//  NSE_WEB_ViewController.swift
//  International Protoype (Tabbed)
//
//  Created by CSE455User on 6/8/15.
//  Copyright (c) 2015 CSE455User. All rights reserved.
//

import UIKit

class NSE_WEB_ViewController: UIViewController {

    @IBOutlet var webview: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let url = NSURL(string: "http://www.nse.org")
        let requestObj = NSURLRequest(URL: url!)
        webview.loadRequest(requestObj)

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
   
    @IBAction func backButton(sender: AnyObject) {
        if webview.canGoBack {
            webview.goBack()
        }
        else {
            dismissViewControllerAnimated(true, completion: nil)
        }
    }

  
    @IBAction func forwardButton(sender: AnyObject) {
        webview.goForward()
    }
    

}
