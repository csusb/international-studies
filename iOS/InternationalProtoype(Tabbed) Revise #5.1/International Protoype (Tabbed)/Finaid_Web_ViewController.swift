//
//  Finaid_Web_ViewController.swift
//  International Protoype (Tabbed)
//
//  Created by CSE455User on 6/8/15.
//  Copyright (c) 2015 CSE455User. All rights reserved.
//

import UIKit

class Finaid_Web_ViewController: UIViewController {

    @IBOutlet var webview: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let url = NSURL(string: "https://finaid.csusb.edu/resources/nationalStudentExchange.html")
        let requestObj = NSURLRequest(URL: url!)
        webview.loadRequest(requestObj)

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButton2(sender: AnyObject) {
        if webview.canGoBack {
            webview.goBack()
        }
        else {
            dismissViewControllerAnimated(true, completion: nil)
        }
    }

    @IBAction func forwardButton2(sender: AnyObject) {
        webview.goForward()
    }
  

}
