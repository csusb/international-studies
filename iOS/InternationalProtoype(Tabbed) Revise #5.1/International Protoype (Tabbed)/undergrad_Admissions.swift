//
//  undergrad_Admissions.swift
//  International Protoype (Tabbed)
//
//  Created by Braden Archibald on 6/1/15.
//  Copyright (c) 2015 CSE455User. All rights reserved.
//

import UIKit

class undergradAdmissionsWV: UIViewController
{
    
    @IBOutlet weak var undergradAdmissionsWV: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let undergradAdmissionsPath:String? = NSBundle.mainBundle().pathForResource("International_Undergrad_Admissions", ofType: "pdf")!
        let url = NSURL(fileURLWithPath: undergradAdmissionsPath!)
        let pdfRequest = NSURLRequest(URL: url)
        self.undergradAdmissionsWV.loadRequest(pdfRequest)
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButton(sender: UIBarButtonItem) {
        dismissViewControllerAnimated(true, completion: nil)
    }
}