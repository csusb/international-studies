//
//  graduateAdmissions.swift
//  International Protoype (Tabbed)
//
//  Created by Braden Archibald on 6/1/15.
//  Copyright (c) 2015 CSE455User. All rights reserved.
//

import UIKit

class graduateAdmissions: UIViewController
{
    
    @IBOutlet weak var gradAdmissionsWV: UIWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let gradAdminpdfPath:String? = NSBundle.mainBundle().pathForResource("Graduate_Admissions", ofType: "pdf")!
        let url = NSURL(fileURLWithPath: gradAdminpdfPath!)
        let pdfRequest = NSURLRequest(URL: url)
        self.gradAdmissionsWV.loadRequest(pdfRequest)
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButton(sender: UIBarButtonItem) {
        dismissViewControllerAnimated(true, completion: nil)
    }
}