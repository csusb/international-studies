//
//  ProgramsAndCourses.swift
//  International Protoype (Tabbed)
//
//  Created by user26236 on 5/13/15.
//  Copyright (c) 2015 CSE455User. All rights reserved.
//

import UIKit

class ProgramsAndCourses: UIViewController {
    
    @IBOutlet weak var CourseListingButton: UIButton!
    
    
    @IBOutlet weak var GeCourseButton: UIButton!
    
    
    @IBOutlet weak var ProgramsTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        CourseListingButton.layer.cornerRadius = 5
        GeCourseButton.layer.cornerRadius = 5
        
        ProgramsTextView.contentOffset = CGPointMake(0, -200)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}